package com.example.hidronqn4.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hidronqn4.model.Medicion

@Database(
    entities = [Medicion::class, RemoteKeys::class],
    version = 1,
    exportSchema = false
)
abstract class MedicionDatabase : RoomDatabase() {
    abstract fun medicionesDao(): MedicionDao
    abstract fun remoteKeysDao(): RemoteKeysDao
}