package com.example.hidronqn4.di

import android.content.Context
import androidx.room.Room
import com.example.hidronqn4.model.local.MedicionDatabase
import com.example.hidronqn4.model.remote.MedicionService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideMedicionDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        MedicionDatabase::class.java,
        "medicion_database"
    ).build()

    @Singleton
    @Provides
    fun provideMedicionDao(db: MedicionDatabase) = db.medicionesDao()

    @Singleton
    @Provides
    fun provideRemoteKeysDao(db: MedicionDatabase) = db.remoteKeysDao()

    @ExperimentalSerializationApi
    @Singleton
    @Provides
    fun provideMedicionService(): MedicionService {
        val contentType = "application/json".toMediaType()
        val json = Json {
            ignoreUnknownKeys = true
            classDiscriminator = "#class"
        }
        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:7000/api/")
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()
        return retrofit.create(MedicionService::class.java)
    }
}