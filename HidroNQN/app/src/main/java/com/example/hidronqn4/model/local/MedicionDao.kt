package com.example.hidronqn4.model.local

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hidronqn4.model.Medicion

@Dao
interface MedicionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(mediciones: List<Medicion>)

    @Query("SELECT * FROM mediciones WHERE dia = :numDia ORDER BY hora ASC")
    fun medicionesByDia(numDia: Int): PagingSource<Int, Medicion>

    @Query("DELETE FROM mediciones WHERE dia = :numDia")
    suspend fun clearMediciones(numDia: Int)
}