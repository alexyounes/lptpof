package com.example.hidronqn4.view

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.ExperimentalPagingApi
import com.example.hidronqn4.R
import com.example.hidronqn4.viewModel.MedicionViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.serialization.ExperimentalSerializationApi

@AndroidEntryPoint
@ExperimentalSerializationApi
@ExperimentalCoroutinesApi
@ExperimentalPagingApi
class MainActivity : AppCompatActivity() {

    private val medicionViewModel: MedicionViewModel by viewModels()

    private fun initObserver() {
        medicionViewModel.message.observe(this, { it ->
            it.getContentIfNotHandled()?.let {
                Toast.makeText(
                    this,
                    it,
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObserver() // Observar excepciones del ViewModel
    }
}