package com.example.hidronqn4.model.remote

import com.example.hidronqn4.model.Medicion
import kotlinx.serialization.Serializable

@Serializable
data class Dia(
    val dia: List<Medicion>
)