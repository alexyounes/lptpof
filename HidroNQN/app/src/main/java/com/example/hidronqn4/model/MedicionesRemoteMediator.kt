package com.example.hidronqn4.model

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.hidronqn4.model.MedicionRepository.Companion.STARTING_PAGE_INDEX
import com.example.hidronqn4.model.local.MedicionDatabase
import com.example.hidronqn4.model.local.RemoteKeys
import com.example.hidronqn4.model.remote.MedicionService
import retrofit2.HttpException
import java.io.IOException

@ExperimentalPagingApi
class MedicionesRemoteMediator(
    private val numDia: Int,
    private val service: MedicionService,
    private val db: MedicionDatabase
) : RemoteMediator<Int, Medicion>() {

    override suspend fun initialize(
    ): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, Medicion>
    ): MediatorResult {

        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                val prevKey = remoteKeys?.prevKey
                if (prevKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                }
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                val nextKey = remoteKeys?.nextKey
                if (nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                }
                nextKey
            }
        }

        try {
            val apiResponse = service.getDiaPaginado(numDia, page, state.config.pageSize)

            val mediciones = apiResponse.dia
            val endOfPaginationReached = mediciones.isEmpty()
            db.withTransaction {
                if (loadType == LoadType.REFRESH) { // Si recarga un día, borrar lo guardado del mismo
                    db.remoteKeysDao().clearRemoteKeys(numDia)
                    db.medicionesDao().clearMediciones(numDia)
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = mediciones.map {
                    RemoteKeys(medId = it.id, numDia = it.dia, prevKey = prevKey, nextKey = nextKey)
                }
                db.remoteKeysDao().insertAll(keys)
                db.medicionesDao().insertAll(mediciones)
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Medicion>): RemoteKeys? {
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { medicion ->
                db.remoteKeysDao().remoteKeysMedicionId(medicion.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Medicion>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { medicion ->
                db.remoteKeysDao().remoteKeysMedicionId(medicion.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Medicion>
    ): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { medId ->
                db.remoteKeysDao().remoteKeysMedicionId(medId)
            }
        }
    }
}