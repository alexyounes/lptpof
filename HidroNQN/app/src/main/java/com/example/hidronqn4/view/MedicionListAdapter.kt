package com.example.hidronqn4.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.hidronqn4.R
import com.example.hidronqn4.model.Medicion
import com.example.hidronqn4.view.MedicionListAdapter.MedicionViewHolder

class MedicionListAdapter :
    PagingDataAdapter<Medicion, MedicionViewHolder>(MedicionesComparator()) {

    override fun onBindViewHolder(holder: MedicionViewHolder, position: Int) {
        val current = getItem(position)
        val dia = current?.dia.toString()
        val hora = current?.hora.toString()
        val temp = current?.temp.toString()
        val hume = current?.hum.toString()
        val prec = current?.prec.toString()
        val uv = current?.uv.toString()
        val altRio = current?.alt.toString()
        val presAtm = current?.pres.toString()
        val velVie = current?.vel.toString()
        val dirVie = current?.dir.toString()
        holder.bind(dia, hora, temp, hume, prec, uv, altRio, presAtm, velVie, dirVie)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicionViewHolder {
        return MedicionViewHolder.getInstance(parent)
    }

    class MedicionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val horaItemView: TextView = itemView.findViewById(R.id.textHoraMed)
        private val tempItemView: TextView = itemView.findViewById(R.id.textTempMed)
        private val humeItemView: TextView = itemView.findViewById(R.id.textHumeMed)
        private val precItemView: TextView = itemView.findViewById(R.id.textPrecMed)
        private val uvItemView: TextView = itemView.findViewById(R.id.textUVMed)
        private val altRioItemView: TextView = itemView.findViewById(R.id.textAltRioMed)
        private val presAtmItemView: TextView = itemView.findViewById(R.id.textPresAtmMed)
        private val velVieItemView: TextView = itemView.findViewById(R.id.textVelVieMed)
        private val dirVieItemView: TextView = itemView.findViewById(R.id.textDirVieMed)

        fun bind(
            dia: String,
            hora: String,
            temp: String,
            hume: String,
            prec: String,
            uv: String,
            altRio: String,
            presAtm: String,
            velVie: String,
            dirVie: String
        ) {
            horaItemView.text = dia
            horaItemView.text = hora
            tempItemView.text = temp
            humeItemView.text = hume
            precItemView.text = prec
            uvItemView.text = uv
            altRioItemView.text = altRio
            presAtmItemView.text = presAtm
            velVieItemView.text = velVie
            dirVieItemView.text = dirVie
        }

        companion object {
            fun getInstance(parent: ViewGroup): MedicionViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val view = inflater.inflate(R.layout.recyclerview_cardview, parent, false)
                return MedicionViewHolder(view)
            }
        }
    }

    class MedicionesComparator : DiffUtil.ItemCallback<Medicion>() {
        override fun areItemsTheSame(oldItem: Medicion, newItem: Medicion): Boolean {
            return oldItem.dia == newItem.dia && oldItem.hora == newItem.hora
        }

        override fun areContentsTheSame(oldItem: Medicion, newItem: Medicion): Boolean {
            return oldItem == newItem
        }
    }
}