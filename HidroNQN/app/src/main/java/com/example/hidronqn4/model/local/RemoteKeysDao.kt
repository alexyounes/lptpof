package com.example.hidronqn4.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RemoteKeysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<RemoteKeys>)

    @Query("SELECT * FROM remote_keys WHERE medId = :medId")
    suspend fun remoteKeysMedicionId(medId: Int): RemoteKeys?

    @Query("DELETE FROM remote_keys WHERE numDia = :numDia")
    suspend fun clearRemoteKeys(numDia: Int)
}