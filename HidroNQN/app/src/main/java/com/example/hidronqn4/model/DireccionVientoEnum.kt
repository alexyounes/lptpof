package com.example.hidronqn4.model

enum class DireccionVientoEnum {
    N, NE, E, SE, S, SO, O, NO;

    companion object {
        fun obtenerValores() = values()
    }
}