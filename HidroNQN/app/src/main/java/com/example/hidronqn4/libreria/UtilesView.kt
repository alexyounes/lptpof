package com.example.hidronqn4.libreria

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.navigation.Navigation
import com.google.android.material.floatingactionbutton.FloatingActionButton

open class UtilesView {
    companion object {
        fun initSpinner(contexto: Context, spinner: Spinner, lista: List<Any>) {
            val adapter = ArrayAdapter(
                contexto,
                android.R.layout.simple_spinner_item,
                lista
            ) // Crear un ArrayAdapter
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // Especificar el esquema usado para la lista de opciones
            spinner.adapter = adapter // Asignar el Adaptador al Spinner
        }

        fun initSpinnerOnItemSelected(spinner: Spinner, funcion: (Int) -> Unit) {
            spinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        funcion(spinner.selectedItem.toString().toInt())
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
        }

        fun initFabConNav(fab: FloatingActionButton, vista: View, destino: Int) {
            fab.setOnClickListener {
                Navigation
                    .findNavController(vista)
                    .navigate(destino)
            }
        }
    }
}