package com.example.hidronqn4.model

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.hidronqn4.model.local.MedicionDatabase
import com.example.hidronqn4.model.remote.Dia
import com.example.hidronqn4.model.remote.MedicionService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ExperimentalPagingApi
class MedicionRepository @Inject constructor(
    private val database: MedicionDatabase,
    private val service: MedicionService
) {
    companion object {
        const val STARTING_PAGE_INDEX = 0
        const val DEFAULT_PAGE_SIZE = 5
    }

    fun letMedicionesFlow(dia: Int): Flow<PagingData<Medicion>> {
        return Pager(
            config = PagingConfig(
                pageSize = DEFAULT_PAGE_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = MedicionesRemoteMediator(
                dia,
                service,
                database
            ),
            pagingSourceFactory = {
                database.medicionesDao().medicionesByDia(dia)
            }
        ).flow
    }

    suspend fun insert(medicion: Medicion) {
        service.insertMedicion(medicion)
    }

    suspend fun getDias(canDias: Int): MutableList<Dia> {
        val dias: MutableList<Dia> = mutableListOf()
        for (numDia in 0..canDias) {
            val diaTemp = service.getDia(numDia)
            dias.add(diaTemp)
        }
        return dias
    }
}