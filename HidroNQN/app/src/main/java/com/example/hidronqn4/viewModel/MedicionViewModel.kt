package com.example.hidronqn4.viewModel

import androidx.lifecycle.*
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.hidronqn4.libreria.Event
import com.example.hidronqn4.model.DireccionVientoEnum
import com.example.hidronqn4.model.Medicion
import com.example.hidronqn4.model.MedicionRepository
import com.example.hidronqn4.model.remote.Dia
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@HiltViewModel
class MedicionViewModel @Inject constructor(
    private val repository: MedicionRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val allDias: List<Int> = (0..10).toList()
    private val statusMessage = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>>
        get() = statusMessage

    // Stream of immutable states representative of the UI.
    val state: StateFlow<UiState>

    // Processor of side effects from the UI which in turn feedback into [state]
    val accept: (UiAction) -> Unit

    init {
        val initialQuery: String = savedStateHandle.get(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
        val lastQueryScrolled: String = savedStateHandle.get(LAST_QUERY_SCROLLED) ?: DEFAULT_QUERY
        val actionStateFlow = MutableSharedFlow<UiAction>()
        val searches = actionStateFlow
            .filterIsInstance<UiAction.Search>()
            .distinctUntilChanged()
            .onStart { emit(UiAction.Search(query = initialQuery)) }
        val queriesScrolled = actionStateFlow
            .filterIsInstance<UiAction.Scroll>()
            .distinctUntilChanged()
            // This is shared to keep the flow "hot" while caching the last query scrolled,
            // otherwise each flatMapLatest invocation would lose the last query scrolled,
            .shareIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
                replay = 1
            )
            .onStart { emit(UiAction.Scroll(currentQuery = lastQueryScrolled)) }

        state = searches
            .flatMapLatest { search ->
                combine(
                    queriesScrolled,
                    searchMediciones(numDia = search.query.toInt()),
                    ::Pair
                )
                    // Each unique PagingData should be submitted once, take the latest from
                    // queriesScrolled
                    .distinctUntilChangedBy { it.second }
                    .map { (scroll, pagingData) ->
                        UiState(
                            query = search.query,
                            pagingData = pagingData,
                            lastQueryScrolled = scroll.currentQuery,
                            // If the search query matches the scroll query, the user has scrolled
                            hasNotScrolledForCurrentSearch = search.query != scroll.currentQuery
                        )
                    }
            }
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
                initialValue = UiState()
            )

        accept = { action ->
            viewModelScope.launch { actionStateFlow.emit(action) }
        }
    }

    override fun onCleared() {
        savedStateHandle[LAST_SEARCH_QUERY] = state.value.query
        savedStateHandle[LAST_QUERY_SCROLLED] = state.value.lastQueryScrolled
        super.onCleared()
    }

    private fun searchMediciones(numDia: Int): Flow<PagingData<Medicion>> =
        repository.letMedicionesFlow(numDia)
            .cachedIn(viewModelScope)

    fun insert(
        id: Int,
        dia: Int,
        hora: String,
        temp: Double,
        hume: Double,
        prec: Double,
        uv: Int,
        altRio: Double,
        presAtm: Double,
        velVie: Double,
        dirVie: String
    ) = viewModelScope.launch {
        val medicionTemp =
            Medicion(id, dia, hora, temp, hume, prec, uv, altRio, presAtm, velVie, dirVie)
        try {
            repository.insert(medicionTemp)
        } catch (exception: IOException) {
            statusMessage.value = Event(exception.toString())
        } catch (exception: HttpException) {
            statusMessage.value = Event(exception.toString())
        }
    }

    fun obtenerDireccionesViento(): ArrayList<String> {
        val direccionesEnum = DireccionVientoEnum.obtenerValores()
        val direccionesString = ArrayList<String>()
        direccionesEnum.forEach { direccionViento -> direccionesString.add(direccionViento.name) }
        return direccionesString
    }

    fun obtenerDias(): List<Int> = allDias

    suspend fun obtenerPromedios(numDias: Int): ArrayList<Double> {
        var dias: MutableList<Dia> = mutableListOf()
        val promedios: ArrayList<Double> = arrayListOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        try {
            dias = repository.getDias(numDias)
        } catch (exception: IOException) {
            statusMessage.value = Event(exception.toString())
        } catch (exception: HttpException) {
            statusMessage.value = Event(exception.toString())
        }
        dias.forEach { dia ->
            val mediciones = dia.dia
            var canMed = 0
            var tempPromDia = 0.0
            var humPromDia = 0.0
            var velPromDia = 0.0
            var canPrecDia = 0.0
            var presPromDia = 0.0
            var altPromDia = 0.0
            var uvPromDia = 0.0
            mediciones.forEach { medicion ->
                canMed++
                presPromDia += medicion.pres
                velPromDia += medicion.vel
                tempPromDia += medicion.temp
                humPromDia += medicion.hum
                canPrecDia += medicion.prec
                altPromDia += medicion.alt
                uvPromDia += medicion.uv
            }
            promedios[0] = promedios[0] + canMed
            promedios[1] = promedios[1] + presPromDia
            promedios[2] = promedios[2] + velPromDia
            promedios[3] = promedios[3] + tempPromDia
            promedios[4] = promedios[4] + humPromDia
            promedios[5] = promedios[5] + altPromDia
            promedios[6] = promedios[6] + uvPromDia
            promedios[7] = promedios[7] + canPrecDia
        }
        promedios[1] = "%.2f".format(promedios[1] / promedios[0]).toDouble()
        promedios[2] = "%.2f".format(promedios[2] / promedios[0]).toDouble()
        promedios[3] = "%.2f".format(promedios[3] / promedios[0]).toDouble()
        promedios[4] = "%.2f".format(promedios[4] / promedios[0]).toDouble()
        promedios[5] = "%.2f".format(promedios[5] / promedios[0]).toDouble()
        promedios[6] = "%.2f".format(promedios[6] / promedios[0]).toDouble()
        promedios[7] = "%.2f".format(promedios[7]).toDouble()
        return promedios
    }

    sealed class UiAction {
        data class Search(val query: String) : UiAction()
        data class Scroll(val currentQuery: String) : UiAction()
    }

    data class UiState(
        val query: String = DEFAULT_QUERY,
        val lastQueryScrolled: String = DEFAULT_QUERY,
        val hasNotScrolledForCurrentSearch: Boolean = false,
        val pagingData: PagingData<Medicion> = PagingData.empty()
    )

    companion object {
        private const val LAST_QUERY_SCROLLED: String = "last_query_scrolled"
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = "0"
    }
}