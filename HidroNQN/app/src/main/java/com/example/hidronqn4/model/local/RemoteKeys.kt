package com.example.hidronqn4.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeys(
    @PrimaryKey val medId: Int,
    val numDia: Int,
    val prevKey: Int?,
    val nextKey: Int?
)