package com.example.hidronqn4.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.hidronqn4.R
import com.example.hidronqn4.databinding.FragmentMedicionListBinding
import com.example.hidronqn4.libreria.UtilesView
import com.example.hidronqn4.viewModel.MedicionViewModel
import com.example.hidronqn4.viewModel.MedicionViewModel.UiAction
import com.example.hidronqn4.viewModel.MedicionViewModel.UiState
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MedicionListFragment : Fragment() {

    private var _binding: FragmentMedicionListBinding? = null
    private val binding get() = _binding!!
    private val medicionViewModel: MedicionViewModel by activityViewModels()

    private lateinit var fabNuevaMed: FloatingActionButton
    private lateinit var fabSelDias: FloatingActionButton

    private fun FragmentMedicionListBinding.bindState(
        uiState: StateFlow<UiState>, // Asociar el UiState de MedicionViewModel a la IU,
        uiActions: (UiAction) -> Unit // y permitir a la IU devolverle las acciones del usuario
    ) {
        val medicionAdapter = MedicionListAdapter()
        val header = MedicionesLoadStateAdapter { medicionAdapter.retry() }
        recyclerview.adapter = medicionAdapter.withLoadStateHeaderAndFooter(
            header = header,
            footer = MedicionesLoadStateAdapter { medicionAdapter.retry() }
        )
        bindSearch(
            uiState = uiState,
            onQueryChanged = uiActions
        )
        bindList(
            header = header,
            medicionAdapter = medicionAdapter,
            uiState = uiState,
            onScrollChanged = uiActions
        )
    }

    private fun FragmentMedicionListBinding.bindSearch(
        uiState: StateFlow<UiState>,
        onQueryChanged: (UiAction.Search) -> Unit
    ) {
        UtilesView.initSpinner(requireContext(), spinnerDiaBuscar, medicionViewModel.obtenerDias())
        spinnerDiaBuscar.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    updateMedicionListFromInput(onQueryChanged)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }

        lifecycleScope.launch {
            uiState
                .map { it.query }
                .distinctUntilChanged()
                .collect()
        }
    }

    private fun FragmentMedicionListBinding.updateMedicionListFromInput(onQueryChanged: (UiAction.Search) -> Unit) {
        val dia = spinnerDiaBuscar.selectedItem.toString()
        recyclerview.scrollToPosition(0)
        onQueryChanged(UiAction.Search(query = dia))
    }

    private fun FragmentMedicionListBinding.bindList(
        header: MedicionesLoadStateAdapter,
        medicionAdapter: MedicionListAdapter,
        uiState: StateFlow<UiState>,
        onScrollChanged: (UiAction.Scroll) -> Unit
    ) {
        botonReintentar.setOnClickListener { medicionAdapter.retry() }
        recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy != 0) onScrollChanged(UiAction.Scroll(currentQuery = uiState.value.query))
            }
        })
        val notLoading = medicionAdapter.loadStateFlow
            .distinctUntilChangedBy { it.refresh } // Cuando REFRESH del mediador remoto cambia
            .map { it.refresh is LoadState.NotLoading } // reaccionar si ya no está cargando

        val hasNotScrolledForCurrentSearch = uiState
            .map { it.hasNotScrolledForCurrentSearch }
            .distinctUntilChanged()

        val shouldScrollToTop = combine(
            notLoading,
            hasNotScrolledForCurrentSearch,
            Boolean::and
        ).distinctUntilChanged()

        val pagingData = uiState
            .map { it.pagingData }
            .distinctUntilChanged()

        lifecycleScope.launch {
            combine(shouldScrollToTop, pagingData, ::Pair)
                // Cada pagingData se envía una sola vez,
                // tomar el último de shouldScrollToTop
                .distinctUntilChangedBy { it.second }
                .collectLatest { (shouldScroll, pagingData) ->
                    medicionAdapter.submitData(pagingData)
                    // Ir arriba solo cuando se envió los datos al adaptador
                    // y es una búsqueda nueva
                    if (shouldScroll) recyclerview.scrollToPosition(0)
                }
        }
        lifecycleScope.launch {
            medicionAdapter.loadStateFlow.collect { loadState ->
                // Mostrar cabecera de reintento si hubo error al refrescar y
                // items fueron previamente guardados Ó volver al estado de preprend por defecto
                header.loadState = loadState.mediator
                    ?.refresh
                    ?.takeIf { it is LoadState.Error && medicionAdapter.itemCount > 0 }
                    ?: loadState.prepend

                val isListEmpty = // Hay lista vacía si no está cargando y no hay items
                    loadState.refresh is LoadState.NotLoading && medicionAdapter.itemCount == 0
                listaVacia.isVisible = isListEmpty
                recyclerview.isVisible = // Mostrar lista solo si refresca con éxito (local o remoto)
                    loadState.source.refresh is LoadState.NotLoading || loadState.mediator?.refresh is LoadState.NotLoading
                barraProgreso.isVisible = // Mostrar barra giratoria durante carga inicial o refresco
                    loadState.mediator?.refresh is LoadState.Loading
                botonReintentar.isVisible = // Mostrar botón reintento si falla carga inicial o refresco
                    loadState.mediator?.refresh is LoadState.Error && medicionAdapter.itemCount == 0
                val errorState = // Mensaje flotante para cualquier error
                    loadState.source.append as? LoadState.Error
                        ?: loadState.source.prepend as? LoadState.Error
                        ?: loadState.append as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error
                errorState?.let {
                    Toast.makeText(
                        requireContext(),
                        "\uD83D\uDE28 Wooops ${it.error}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMedicionListBinding.inflate(inflater, container, false)
        binding.bindState(
            uiState = medicionViewModel.state,
            uiActions = medicionViewModel.accept
        )
        return binding.root // Vista
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fabNuevaMed = binding.fabNuevaMed
        fabSelDias = binding.fabSelDia
        UtilesView.initFabConNav(fabNuevaMed, view, R.id.action_medicionListFragment_to_newMedicionFragment)
        UtilesView.initFabConNav(fabSelDias, view, R.id.action_medicionListFragment_to_diasFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}