package com.example.hidronqn4.libreria

open class Event<out T>(private val content: T) {

    private var _hasBeenHandled = false
    private val hasBeenHandled get() = _hasBeenHandled

    // Retornar contenido y prevenir que sea usado de nuevo
    fun getContentIfNotHandled(): T? {
        return if (_hasBeenHandled) {
            null
        } else {
            _hasBeenHandled = true
            content
        }
    }

    // Retornar contenido, incluso si ya fue usado
    fun peekContent(): T = content
}