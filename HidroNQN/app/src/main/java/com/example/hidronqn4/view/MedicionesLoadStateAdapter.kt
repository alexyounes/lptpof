package com.example.hidronqn4.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.hidronqn4.R
import com.example.hidronqn4.databinding.RecyclerviewFooterBinding
import com.example.hidronqn4.view.MedicionesLoadStateAdapter.MedicionesLoadStateViewHolder

class MedicionesLoadStateAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<MedicionesLoadStateViewHolder>() {

    override fun onBindViewHolder(
        holder: MedicionesLoadStateViewHolder,
        loadState: LoadState
    ) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): MedicionesLoadStateViewHolder {
        return MedicionesLoadStateViewHolder.create(parent, retry)
    }

    class MedicionesLoadStateViewHolder(
        private val binding: RecyclerviewFooterBinding,
        retry: () -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.botonReintentar.setOnClickListener { retry.invoke() }
        }

        fun bind(loadState: LoadState) {
            if (loadState is LoadState.Error) {
                binding.mensajeError.text = loadState.error.localizedMessage
            }
            binding.barraProgreso.isVisible = loadState is LoadState.Loading
            binding.botonReintentar.isVisible = loadState is LoadState.Error
            binding.mensajeError.isVisible = loadState is LoadState.Error
        }

        companion object {
            fun create(
                parent: ViewGroup,
                retry: () -> Unit
            ): MedicionesLoadStateViewHolder {
                val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.recyclerview_footer, parent, false)
                val binding = RecyclerviewFooterBinding.bind(view)
                return MedicionesLoadStateViewHolder(binding, retry)
            }
        }
    }
}