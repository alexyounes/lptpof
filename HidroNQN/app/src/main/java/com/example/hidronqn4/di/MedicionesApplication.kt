package com.example.hidronqn4.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MedicionesApplication : Application()