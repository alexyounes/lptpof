package com.example.hidronqn4.view

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import com.example.hidronqn4.R
import com.example.hidronqn4.databinding.FragmentNewMedicionBinding
import com.example.hidronqn4.libreria.UtilesView
import com.example.hidronqn4.viewModel.MedicionViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
class NewMedicionFragment : Fragment() {

    private val medicionViewModel: MedicionViewModel by activityViewModels()

    private lateinit var spinnerDia: Spinner
    private lateinit var spinnerDirVie: Spinner
    private lateinit var editHora: EditText
    private lateinit var editUV: EditText
    private lateinit var editTemp: EditText
    private lateinit var editPres: EditText
    private lateinit var editPrec: EditText
    private lateinit var editVel: EditText
    private lateinit var editAlt: EditText
    private lateinit var editHume: EditText
    private lateinit var buttonGuardar: Button

    private var _binding: FragmentNewMedicionBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNewMedicionBinding.inflate(inflater, container, false)
        return binding.root // vista
    }

    private fun initBinding() {
        spinnerDia = binding.spinnerDia
        spinnerDirVie = binding.spinnerDirVie
        editHora = binding.editHora
        editUV = binding.editUv
        editTemp = binding.editTemp
        editPres = binding.editPres
        editPrec = binding.editPrec
        editVel = binding.editVel
        editAlt = binding.editAlt
        editHume = binding.editHume
        buttonGuardar = binding.buttonGuardar
    }

    private fun initBotonGuardar() {
        buttonGuardar.setOnClickListener {
            if (
                TextUtils.isEmpty(editHora.text) ||
                TextUtils.isEmpty(editUV.text) ||
                TextUtils.isEmpty(editTemp.text) ||
                TextUtils.isEmpty(editPres.text) ||
                TextUtils.isEmpty(editPrec.text) ||
                TextUtils.isEmpty(editVel.text) ||
                TextUtils.isEmpty(editAlt.text) ||
                TextUtils.isEmpty(editHume.text)
            ) {
                Toast.makeText(
                    requireContext(),
                    R.string.vacio_no_guardado,
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val hora = editHora.text.toString()
                val uv = editUV.text.toString().toInt()
                val temp = editTemp.text.toString().toDouble()
                val pres = editPres.text.toString().toDouble()
                val prec = editPrec.text.toString().toDouble()
                val vel = editVel.text.toString().toDouble()
                val alt = editAlt.text.toString().toDouble()
                val hume = editHume.text.toString().toDouble()
                val dia = spinnerDia.selectedItem.toString().toInt()
                val dir = spinnerDirVie.selectedItem.toString()
                medicionViewModel.insert(
                    dia + hora.hashCode(),
                    dia,
                    hora,
                    temp,
                    hume,
                    prec,
                    uv,
                    alt,
                    pres,
                    vel,
                    dir
                )
                Toast.makeText(
                    requireContext(),
                    R.string.guardado,
                    Toast.LENGTH_LONG
                ).show()
                findNavController().navigateUp()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initBinding()
        UtilesView.initSpinner(requireContext(), spinnerDia, medicionViewModel.obtenerDias())
        UtilesView.initSpinner(
            requireContext(),
            spinnerDirVie,
            medicionViewModel.obtenerDireccionesViento()
        )
        initBotonGuardar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}