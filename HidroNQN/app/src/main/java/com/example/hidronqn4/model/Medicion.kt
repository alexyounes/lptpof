package com.example.hidronqn4.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import kotlinx.serialization.Serializable

@Entity(
    tableName = "mediciones",
    primaryKeys = ["dia", "hora"]
)

@Serializable
data class Medicion(
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "dia") val dia: Int,
    @ColumnInfo(name = "hora") val hora: String,
    @ColumnInfo(name = "temp") val temp: Double,
    @ColumnInfo(name = "hume") val hum: Double,
    @ColumnInfo(name = "prec") val prec: Double,
    @ColumnInfo(name = "uv") val uv: Int,
    @ColumnInfo(name = "altRio") val alt: Double,
    @ColumnInfo(name = "presAtm") val pres: Double,
    @ColumnInfo(name = "velVie") val vel: Double,
    @ColumnInfo(name = "dirVie") val dir: String
)