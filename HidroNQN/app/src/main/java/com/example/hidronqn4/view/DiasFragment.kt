package com.example.hidronqn4.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import com.example.hidronqn4.databinding.FragmentDiasBinding
import com.example.hidronqn4.libreria.UtilesView
import com.example.hidronqn4.viewModel.MedicionViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
class DiasFragment : Fragment() {

    private val medicionViewModel: MedicionViewModel by activityViewModels()
    private var _binding: FragmentDiasBinding? = null
    private val binding get() = _binding!!

    private lateinit var spinnerDia: Spinner
    private lateinit var textCanMed: TextView
    private lateinit var canMed: TextView
    private lateinit var textPromUv: TextView
    private lateinit var promUv: TextView
    private lateinit var textPromTemp: TextView
    private lateinit var promTemp: TextView
    private lateinit var textCanPrec: TextView
    private lateinit var canPrec: TextView
    private lateinit var textPromAlt: TextView
    private lateinit var promAlt: TextView
    private lateinit var textPromPres: TextView
    private lateinit var promPres: TextView
    private lateinit var textPromVel: TextView
    private lateinit var promVel: TextView
    private lateinit var textPromHum: TextView
    private lateinit var promHum: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDiasBinding.inflate(inflater, container, false)
        return binding.root // vista
    }

    private fun initBinding() {
        spinnerDia = binding.spinnerCanDias
        textCanMed = binding.textCanMed
        canMed = binding.canMediciones
        textPromUv = binding.textPromUv
        promUv = binding.promUv
        textPromTemp = binding.textPromTemp
        promTemp = binding.promTemp
        textCanPrec = binding.textCanPrec
        canPrec = binding.canPrec
        textPromAlt = binding.textPromAlt
        promAlt = binding.promAlt
        textPromPres = binding.textPromPres
        promPres = binding.promPres
        textPromVel = binding.textPromVel
        promVel = binding.promVel
        textPromHum = binding.textPromHum
        promHum = binding.promHum
    }

    private fun selNDias(numDias: Int) {
        lifecycleScope.launch {
            val promedios = medicionViewModel.obtenerPromedios(numDias)
            canMed.text = promedios[0].toInt().toString()
            promPres.text = promedios[1].toString()
            promVel.text = promedios[2].toString()
            promTemp.text = promedios[3].toString()
            promHum.text = promedios[4].toString()
            promAlt.text = promedios[5].toString()
            promUv.text = promedios[6].toString()
            canPrec.text = promedios[7].toString()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initBinding()
        UtilesView.initSpinner(requireContext(), spinnerDia, medicionViewModel.obtenerDias())
        UtilesView.initSpinnerOnItemSelected(spinnerDia, ::selNDias)
        // Cargar promedios de hoy
        selNDias(0)
    }
}