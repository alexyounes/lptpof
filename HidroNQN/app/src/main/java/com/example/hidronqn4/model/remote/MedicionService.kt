package com.example.hidronqn4.model.remote

import com.example.hidronqn4.model.Medicion
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface MedicionService {
    @GET("mediciones&numDia={numDia}&numPag={numPag}&cantidad={cantidad}")
    suspend fun getDiaPaginado(
        @Path("numDia") numDia: Int,
        @Path("numPag") numPag: Int,
        @Path("cantidad") cantidad: Int
    ): Dia

    @POST("mediciones")
    suspend fun insertMedicion(
        @Body medicion: Medicion
    ): Medicion

    @GET("mediciones&numDia={numDia}")
    suspend fun getDia(
        @Path("numDia") numDia: Int
    ): Dia
}