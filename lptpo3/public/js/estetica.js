// Quita "active" de los navItem y la agrega al navItem de elemento
function navItemActivo(elemento) {
    const navItems = DOM.getElementsByClassName("nav-item")
    for (navItem of navItems) {
        navItem.classList.remove("active")
    }
    elemento.closest(".nav-item").classList.add("active")
}