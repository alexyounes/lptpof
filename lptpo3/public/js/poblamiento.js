// DOM de la página
const DOM = document
// Cuando la página esté cargada, pedir los datos de hoy
const posSepNumDia = DOM.URL.indexOf('-')
const posIniNumDia = posSepNumDia + 1
const posFinNumDia = DOM.URL.length
const numDia = DOM.URL.substring(posIniNumDia, posFinNumDia)
DOM.addEventListener('DOMContentLoaded', () => { obtenerMedicionesDelDia(DOM.getElementById(numDia), numDia) })
// Vaciar contenido de un nodo HTML
function vaciar(nodo) {
  nodo.innerHTML = ""
}
// Valores para la fila cabecera
const valorCeldaCabeza = ["Hora", "UV", "Temp. ºC",
  "Hum. Rel. %", "Vel. Vie. Km/h", "Dir. Vie.", "Pres. Atm. hPa",
  "Prec. mm", "Alt. Río m"]
// Crear los elementos para mostrar un día completo
// Recibe por parametro los datos y el número del día
function crearFila({ fecha, hora, uv, temp, hum, vel, dir, pres, prec, alt }, i) {
  let nuevoCuerpo
  const valorCeldaDato = [hora, uv, temp, hum, vel, dir, pres, prec, alt]
  const nuevaFilaCuerpo = DOM.createElement("tr")
  if (i === 0) { // Si no hay cabecera, la agrega y luego la fila
    const nuevaColumna = DOM.createElement("div")
    nuevaColumna.className = "col-md-12"
    const nuevoTitulo = DOM.createElement("h3")
    const nuevoParrafo = DOM.createElement("p")
    nuevoParrafo.className = "text-center"
    const nuevaNegrita = DOM.createElement("b")
    const nuevoTexto = DOM.createTextNode("Datos del Día " + fecha)
    nuevaNegrita.appendChild(nuevoTexto)
    nuevoParrafo.appendChild(nuevaNegrita)
    nuevoTitulo.appendChild(nuevoParrafo)
    nuevaColumna.appendChild(nuevoTitulo)
    const nuevaTabla = DOM.createElement("table")
    nuevaTabla.className = "table table-sm table-bordered table-hover"
    const nuevaCabeza = DOM.createElement("thead")
    const nuevaFilaCabeza = DOM.createElement("tr")
    valorCeldaCabeza.forEach(valor => {
      const nuevaCeldaCabeza = DOM.createElement("th")
      nuevaCeldaCabeza.scope = "col"
      const nuevoTexto = DOM.createTextNode(valor)
      nuevaCeldaCabeza.appendChild(nuevoTexto)
      nuevaFilaCabeza.appendChild(nuevaCeldaCabeza)
    })
    nuevaCabeza.appendChild(nuevaFilaCabeza)
    nuevoCuerpo = DOM.createElement("tbody")
    nuevoCuerpo.id = "cuerpoTabla"
    crearFilaCuerpo(nuevoCuerpo, valorCeldaDato, nuevaFilaCuerpo)
    nuevaTabla.appendChild(nuevaCabeza)
    nuevaTabla.appendChild(nuevoCuerpo)
    nuevaColumna.appendChild(nuevaTabla)
    DOM.getElementById("contenedor-datos").appendChild(nuevaColumna)
  } else { // Si ya hay cabecera, agrega la fila
    nuevoCuerpo = DOM.getElementById("cuerpoTabla")
    crearFilaCuerpo(nuevoCuerpo, valorCeldaDato, nuevaFilaCuerpo)
  }
}
// Método que crea nueva fila del cuerpo
function crearFilaCuerpo(nuevoCuerpo, valorCeldaDato, nuevaFilaCuerpo) {
  valorCeldaDato.forEach(valor => {
    const nuevaCeldaDato = DOM.createElement("td")
    const nuevoTexto = DOM.createTextNode(valor)
    nuevaCeldaDato.appendChild(nuevoTexto)
    nuevaFilaCuerpo.appendChild(nuevaCeldaDato)
  })
  nuevoCuerpo.appendChild(nuevaFilaCuerpo)
}
// Cambia el número de la pagina
function setearNumPagina(elemento, dia) {
  let numeroPagina
  if (dia === 0) {
    numeroPagina = DOM.createTextNode("Hoy")
    elemento = DOM.getElementById("home")
  } else {
    numeroPagina = DOM.createTextNode("Día -" + dia)
  }
  return numeroPagina
}
// Solicitar mediciones del día
async function obtenerMedicionesDelDia(elemento, numDia) {
  let mediciones = await solicitarMedicionesDelDia(numDia)
  mediciones = mediciones.dia
  vaciar(DOM.getElementById("contenedor-datos"))
  const nombrePagina = DOM.getElementById("pagina")
  vaciar(nombrePagina)
  const numeroPagina = setearNumPagina(elemento, numDia)
  nombrePagina.appendChild(numeroPagina)
  mediciones.forEach((medicion,i) => {crearFila(medicion,i)})
  navItemActivo(elemento)
}