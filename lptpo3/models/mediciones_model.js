// Devolver el tipo esperado para un atributo
function tipoEsperado(atributo) {
    // SE PUEDE ACHICAR CON UN ARREGLO DE MAPAS
    // Libreria JOI
    let debeSer;
    switch (atributo) {
        case "id": debeSer = "number"; break;
        case "fecha": debeSer = "string"; break;
        case "hora": debeSer = "string"; break;
        case "uv": debeSer = "number"; break;
        case "temp": debeSer = "number"; break;
        case "hum": debeSer = "number"; break;
        case "vel": debeSer = "number"; break;
        case "dir": debeSer = "string"; break;
        case "pres": debeSer = "number"; break;
        case "prec": debeSer = "number"; break;
        case "alt": debeSer = "number"; break;
        case "dia": debeSer = "number"; break;
    }
    return debeSer;
}

module.exports = {
    tipoEsperado: tipoEsperado
};