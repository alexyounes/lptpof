// Modelo de la BD
const validador = require('../models/mediciones_model')
// Cargar la BD desde un archivo
const filas = require("../database/mediciones_database")
// Validar atributos de la medición
function validarMedicion(medicion) {
    let atributosIncorrectos = []
    let i = 0
    for (let atributo in medicion) {
        if (typeof medicion[atributo] != validador.tipoEsperado(atributo)) {
            atributosIncorrectos[i] = atributo
            i++
        }
    }
    return atributosIncorrectos
}
// Insertar medición recibida
function insertarMedicion(medicion) {
    const { id, fecha, hora, uv, temp, hum, vel, dir, pres, prec, alt, dia } = medicion
    const fila = { id, fecha, hora, uv, temp, hum, vel, dir, pres, prec, alt, dia }
    filas.push(fila)
    return fila
}
// Quitar medición existente
function quitarMedicion(medicion) {
    const indiceAEliminar = filas.indexOf(medicion)
    filas.splice(indiceAEliminar, 1)
}
// Modificar medición existente usando nuevos datos
function modificarMedicion(medicion, nuevosDatos) {
    const { fecha, hora, uv, temp, hum, vel, dir, pres, prec, alt, dia } = nuevosDatos
    medicion.fecha = fecha
    medicion.hora = hora
    medicion.uv = uv
    medicion.temp = temp
    medicion.hum = hum
    medicion.vel = vel
    medicion.dir = dir
    medicion.pres = pres
    medicion.prec = prec
    medicion.alt = alt
    medicion.dia = dia
    return medicion
}
// Traer una medición por su ID
function medicionPorID(id) {
    return filas.find((fila) => fila.id == id)
}
// Obtener todas las mediciones del día
function medicionesDelDia(dia) {
    return filas.filter((fila) => fila.dia == dia)
}
// Obtener mediciones en rango de IDs
function medicionesEntreIDs(desde, cantidad) {
    return filas.slice(desde, desde + cantidad)
}
// Obtener mediciones paginadas de un día
function medicionesDiaPaginada(numDia, numPag, cantidad) {
    const ini = numPag*cantidad
    const fin = ini + cantidad
    return filas.filter((fila) => fila.dia == numDia).slice(ini,fin)
}
// Funciones públicas
module.exports = {
    validarMedicion: validarMedicion,
    insertarMedicion: insertarMedicion,
    medicionPorID: medicionPorID,
    medicionesDelDia: medicionesDelDia,
    medicionesEntreIDs: medicionesEntreIDs,
    quitarMedicion: quitarMedicion,
    modificarMedicion: modificarMedicion,
    medicionesDiaPaginada: medicionesDiaPaginada
}