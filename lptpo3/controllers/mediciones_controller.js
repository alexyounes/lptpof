// Servicios para acceder a la BD
const service = require("../services/mediciones_service")
// Obtener una fila por su ID
function obtenerMedicion(solicitud, respuesta) {
  const id = parseInt(solicitud.params.id)
  if (id >= 0) {
    const medicion = service.medicionPorID(id)
    if (medicion) {
      crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", medicion)
    } else {
      crearRespuesta(respuesta, 404, "Contenido no encontrado")
    }
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida")
  }
}
// Obtener Filas desde un ID hasta cierta cantidad
function obtenerMedicionesEntreIDs(solicitud, respuesta) {
  const desde = parseInt(solicitud.params.desde)
  const cantidad = parseInt(solicitud.params.cantidad)
  if (desde >= 0 && cantidad > 0) {
    const resultado = service.medicionesEntreIDs(desde,cantidad)
    crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", resultado)
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida")
  }
}

function obtenerMedicionesDiaPaginada(solicitud, respuesta) {
  const numDia = parseInt(solicitud.params.numDia)
  const numPag = parseInt(solicitud.params.numPag)
  const cantidad = parseInt(solicitud.params.cantidad)
  if (numPag >= 0 && cantidad > 0) {
    let dia = service.medicionesDiaPaginada(numDia,numPag,cantidad)
    dia = Object.assign({dia})
    crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", dia)
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida")
  }
}
// Obtener todas las filas de un dia
function obtenerMedicionesDelDia(solicitud, respuesta) {
  let dia = service.medicionesDelDia(solicitud.params.dia)
  dia = Object.assign({dia})
  crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", dia)
}
// Solicitar agregar una medición nueva
function agregarMedicion(solicitud, respuesta) {
  console.log("asd")
  let atributosIncorrectos = service.validarMedicion(solicitud.body)
  if (atributosIncorrectos.length == 0) {
    const medicion = service.insertarMedicion(solicitud.body)
    crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", medicion)
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida", "Valores incorrectos en:" + atributosIncorrectos)
  }
}
// Eliminar una fila existente
function eliminarMedicion(solicitud, respuesta) {
  const id = parseInt(solicitud.params.id)
  if (id >= 0) {
    const medicion = service.medicionPorID(id)
    if (medicion) {
      service.quitarMedicion(medicion)
      crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", medicion)
    } else {
      crearRespuesta(respuesta, 404, "Contenido no encontrado")
    }
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida")
  }
}
// Actualizar una fila existente
function actualizarMedicion(solicitud, respuesta) {
  let atributosIncorrectos = service.validarMedicion(solicitud.body)
  if (atributosIncorrectos.length == 0) {
    let medicion = service.medicionPorID(solicitud.params.id)
    if (medicion) {
      medicion = service.modificarMedicion(medicion, solicitud.body)
      crearRespuesta(respuesta, 200, "La solicitud ha tenido éxito", medicion)
    } else {
      crearRespuesta(respuesta, 404, "Contenido no encontrado")
    }
  } else {
    crearRespuesta(respuesta, 400, "Solicitud inválida", "Valores incorrectos en:" + atributosIncorrectos)
  }
}
// Crear una respuesta HTML
function crearRespuesta(respuesta, codigo, mensaje) {
  respuesta.status(codigo).json({mensaje})
}
function crearRespuesta(respuesta, codigo, mensaje, retorno) {
  console.log(retorno)
  retorno = Object.assign({mensaje},retorno)
  respuesta.status(codigo).json(retorno)
}
// Funciones públicas
module.exports = {
  agregarMedicion: agregarMedicion,
  obtenerMedicion: obtenerMedicion,
  obtenerMedicionesEntreIDs: obtenerMedicionesEntreIDs,
  obtenerMedicionesDiaPaginada: obtenerMedicionesDiaPaginada,
  actualizarMedicion: actualizarMedicion,
  eliminarMedicion: eliminarMedicion,
  obtenerMedicionesDelDia: obtenerMedicionesDelDia,
}